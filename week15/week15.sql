use movie_db;

alter table countries drop primary key;
alter table directors drop primary key;
alter table movies drop primary key;
alter table stars drop primary key;
alter table movie_stars drop primary key;
alter table movie_directors drop primary key;
alter table languages drop primary key;
alter table genres drop primary key;
alter table producer_countries drop primary key;

load data local infile 'C:\Users\PC\Desktop\movies.csv' into table movies fields terminated by ';';
load data local infile 'C:\Users\PC\Desktop\directors.csv' into table movies fields terminated by ';';
load data local infile 'C:\Users\PC\Desktop\stars.csv' into table movies fields terminated by ';';
load data local infile 'C:\Users\PC\Desktop\countries.csv' into table movies fields terminated by ';';
load data local infile 'C:\Users\PC\Desktop\languages.csv' into table movies fields terminated by ';';
load data local infile 'C:\Users\PC\Desktop\genres.csv' into table movies fields terminated by ';';
load data local infile 'C:\Users\PC\Desktop\movie_directors.csv' into table movies fields terminated by ';';
load data local infile 'C:\Users\PC\Desktop\movie_stars.csv' into table movies fields terminated by ';';
load data local infile 'C:\Users\PC\Desktop\producer_countries.csv' into table movies fields terminated by ';';

select*from movies;

alter table movies add primary key(movie_id);
alter table directors add primary key(director_id);
alter table stars add primary key(star_id);
alter table countries add primary key(country_id);
alter table languages add primary key(movie_id, language_name);
alter table genres add primary key(movie_id, genre_name);
alter table movie_stars add primary key(movie_id, star_id);
alter table movie_directors add primary key(movie_id, director_id);
alter table producer_countries add primary key(movie_id, country_id);

alter table directors add constraint fk_directors_countries1 foreign key(country_id) references countries(country_id);
alter table stars add constraint fk_stars_countries1 foreign key(country_id) references countries(country_id);
alter table languages add constraint fk_languages_movies1 foreign key(movie_id) references movies(movie_id);
alter table genres add constraint fk_genres_movies1 foreign key(movie_id) references movies(movie_id);
alter table movie_stars add constraint fk_movie_stars_movies1 foreign key(movie_id) references movies(movie_id);
alter table movie_stars add constraint fk_movie_stars_stars1 foreign key(star_id) references stars(star_id);
alter table movie_directors add constraint fk_movie_directors_movies1 foreign key(movie_id) references movies(movie_id);
alter table movie_directors add constraint fk_movie_directors_directors1 foreign key(director_id) references directors(director_id);
alter table producer_countries add constraint fk_producer_countries_countries1 foreign key(country_id) references countries(country_id);
alter table producer_countries add constraint fk_producer_countries_movies1 foreign key(movie_id) references movies(movie_id);

CREATE INDEX `fk_producer_countries_movies_idx` ON `producer_countries` (`movie_id` ASC);
CREATE INDEX `fk_producer_countries_countries1_idx` ON `producer_countries` (`country_id` ASC);
CREATE INDEX `fk_languages_movies1_idx` ON `languages` (`movie_id` ASC);
CREATE INDEX `fk_directors_countries1_idx` ON `directors` (`country_id` ASC);
CREATE INDEX `fk_movie_directors_movies1_idx` ON `movie_directors` (`movie_id` ASC);
CREATE INDEX `fk_movie_directors_directors1_idx` ON `movie_directors` (`director_id` ASC);
CREATE INDEX `fk_stars_countries1_idx` ON `stars` (`country_id` ASC);
CREATE INDEX `fk_movie_stars_movies1_idx` ON `movie_stars` (`movie_id` ASC);
CREATE INDEX `fk_movie_stars_stars1_idx` ON `movie_stars` (`star_id` ASC);
CREATE INDEX `fk_genres_movies1_idx` ON `genres` (`movie_id` ASC);