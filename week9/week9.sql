use bank;

select *
from loan, borrower;

select *
from table1 join table2 on table1.loan_number = table2.loan_number;

select *
from loan left join borrower on loan.loan_number = borrower.loan_number;

insert into borrower values ("Erdem2","L-38");

select *
from loan right join borrower on loan.loan_number = borrower.loan_number;

(select loan.loan_number customer_name
from loan left join borrower on loan.loan_number = borrower.loan_number)
union
(select loan.loan_number customer_name
from loan right join borrower on loan.loan_number = borrower.loan_number);
